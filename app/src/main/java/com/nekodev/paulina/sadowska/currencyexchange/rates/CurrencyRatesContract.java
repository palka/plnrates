package com.nekodev.paulina.sadowska.currencyexchange.rates;

import com.nekodev.paulina.sadowska.currencyexchange.BasePresenter;
import com.nekodev.paulina.sadowska.currencyexchange.BaseView;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewType;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;

import java.util.List;

/**
 * Created by Paulina Sadowska on 04.08.2017.
 */

public interface CurrencyRatesContract {
    interface View extends BaseView<Presenter> {

        void addCurrencyRates(String date, List<CurrencyItem> currencies);

        void showError();

        void onResultEmpty();

        void openCurrencyRateDetails(CurrencyItem currencyItem);

        void setTitle(String title);

        void clearCurrencyList();
    }

    interface Presenter extends BasePresenter {

        void getNextRates();


        void currencyItemSelected(CurrencyItem currencyItem);

        void addCachedRates();

        void scrolledToItem(ViewType item);

        void refresh();
    }
}
