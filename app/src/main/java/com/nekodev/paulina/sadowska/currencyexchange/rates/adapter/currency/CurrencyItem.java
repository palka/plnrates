package com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency;

import android.os.Parcel;
import android.os.Parcelable;

import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.AdapterConstants;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewType;

/**
 * Created by Paulina Sadowska on 05.08.2017.
 */

public class CurrencyItem implements ViewType, Parcelable {

    private final String name;
    private final Double rate;
    private final String date;

    public CurrencyItem(String date, String name, Double rate) {
        this.name = name;
        this.rate = rate;
        this.date = date;
    }

    @SuppressWarnings("WeakerAccess")
    protected CurrencyItem(Parcel in) {
        name = in.readString();
        date = in.readString();
        rate = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(date);
        dest.writeDouble(rate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CurrencyItem> CREATOR = new Creator<CurrencyItem>() {
        @Override
        public CurrencyItem createFromParcel(Parcel in) {
            return new CurrencyItem(in);
        }

        @Override
        public CurrencyItem[] newArray(int size) {
            return new CurrencyItem[size];
        }
    };

    public Double getRate() {
        return rate;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    @Override
    public int getViewType() {
        return AdapterConstants.CURRENCY;
    }
}
