package com.nekodev.paulina.sadowska.currencyexchange.data;

import com.nekodev.paulina.sadowska.currencyexchange.api.FixerService;
import com.nekodev.paulina.sadowska.currencyexchange.data.local.Local;
import com.nekodev.paulina.sadowska.currencyexchange.data.local.RatesLocalDataSource;
import com.nekodev.paulina.sadowska.currencyexchange.data.remote.RatesRemoteDataSource;
import com.nekodev.paulina.sadowska.currencyexchange.data.remote.Remote;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Paulina Sadowska on 04.08.2017.
 */

@Module
class RatesRepositoryModule {

    private static final String HOST = "http://api.fixer.io/";

    @Provides
    @Singleton
    @Remote
    RatesDataSource provideTasksRemoteDataSource(FixerService fixerService) {
        return new RatesRemoteDataSource(fixerService);
    }

    @Provides
    @Singleton
    @Local
    RatesDataSource provideTasksLocalDataSource() {
        return new RatesLocalDataSource();
    }

    @Provides
    @Singleton
    FixerService providesFixerService(Retrofit retrofit) {
        return retrofit.create(FixerService.class);
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient providerOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        return httpClient.build();
    }
}
