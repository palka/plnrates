package com.nekodev.paulina.sadowska.currencyexchange;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 07.08.2017.
 */

@Module
public final class ApplicationModule {

    private final Context mContext;

    ApplicationModule(Context context) {
        mContext = context;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }
}
