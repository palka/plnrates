package com.nekodev.paulina.sadowska.currencyexchange.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Paulina Sadowska on 06.08.2017.
 */

public class InfiniteScrollListener extends RecyclerView.OnScrollListener {

    private int previousTotal;
    private boolean loading;
    private int visibleThreshold;
    private final OnEndReachedListener endReachedListener;

    public InfiniteScrollListener(OnEndReachedListener endReachedListener) {
        this.endReachedListener = endReachedListener;
        this.visibleThreshold = 2;
        this.previousTotal = 0;
        this.loading = true;
    }


    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (dy > 0) {
            int visibleItemCount = recyclerView.getChildCount();
            int totalItemCount = recyclerView.getLayoutManager().getItemCount();
            int firstVisibleItem = findFirstVisibleItemPosition(recyclerView.getLayoutManager());
            if (firstVisibleItem < 0) {
                return;
            }

            if (loading && totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            } else if (!loading && (totalItemCount - visibleItemCount)
                    <= (firstVisibleItem + visibleThreshold)) {
                endReachedListener.onEndReached();
                loading = true;

            }
        }
    }

    private int findFirstVisibleItemPosition(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        }
        return -1;
    }

    public void setVisibleThreshold(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }

    public interface OnEndReachedListener {
        void onEndReached();
    }
}
