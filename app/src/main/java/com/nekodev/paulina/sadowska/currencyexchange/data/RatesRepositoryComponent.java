package com.nekodev.paulina.sadowska.currencyexchange.data;

import com.nekodev.paulina.sadowska.currencyexchange.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Paulina Sadowska on 04.08.2017.
 */
@Singleton
@Component(modules = {RatesRepositoryModule.class, ApplicationModule.class})
public interface RatesRepositoryComponent {
    RatesRepository getRatesRepository();
}