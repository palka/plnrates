package com.nekodev.paulina.sadowska.currencyexchange;

/**
 * Created by Paulina Sadowska on 04.08.2017.
 */

public interface BasePresenter {
    void subscribe();

    void unSubscribe();
}
