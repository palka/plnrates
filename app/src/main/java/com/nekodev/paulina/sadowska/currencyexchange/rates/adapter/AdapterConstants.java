package com.nekodev.paulina.sadowska.currencyexchange.rates.adapter;

/**
 * Created by Paulina Sadowska on 05.08.2017.
 */
public class AdapterConstants {
    public static final int LOADING = 1;
    public static final int DATE = 2;
    public static final int CURRENCY = 3;
    public static final int ERROR = 4;

    private AdapterConstants() {
    }
}
