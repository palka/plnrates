package com.nekodev.paulina.sadowska.currencyexchange.rates;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nekodev.paulina.sadowska.currencyexchange.BaseFragment;
import com.nekodev.paulina.sadowska.currencyexchange.R;
import com.nekodev.paulina.sadowska.currencyexchange.ratedetails.RateDetailsActivity;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.CurrencyRatesAdapter;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewType;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.date.DateItem;
import com.nekodev.paulina.sadowska.currencyexchange.utils.InfiniteScrollListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 04.08.2017.
 */

public class CurrencyRatesFragment extends BaseFragment<CurrencyRatesContract.Presenter> implements CurrencyRatesContract.View {

    private static final String BUNDLE_RECYCLER_LAYOUT = "classname.recycler.layout";

    @BindView(R.id.rates_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.rates_swiperefresh)
    SwipeRefreshLayout mSwipeRefresh;

    public static CurrencyRatesFragment newInstance() {
        return new CurrencyRatesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.currency_rates_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(new CurrencyRatesAdapter(currencyItem -> mPresenter.currencyItemSelected(currencyItem)));
            mPresenter.addCachedRates();
        }
        if (savedInstanceState != null) {
            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
            mRecyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);
        }
        mRecyclerView.setOnScrollListener(new InfiniteScrollListener(() -> mPresenter.getNextRates()));
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                ViewType item = ((CurrencyRatesAdapter) mRecyclerView.getAdapter())
                        .getItem(((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition());
                mPresenter.scrolledToItem(item);
            }
        });
        mSwipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        mSwipeRefresh.setOnRefreshListener(() -> mPresenter.refresh());
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void addCurrencyRates(String date, List<CurrencyItem> currencies) {
        mSwipeRefresh.setRefreshing(false);
        ((CurrencyRatesAdapter) mRecyclerView.getAdapter()).addItems(new DateItem(date), currencies);
    }

    @Override
    public void showError() {
        if (mRecyclerView.getAdapter() != null) {
            mSwipeRefresh.setRefreshing(false);
            ((CurrencyRatesAdapter) mRecyclerView.getAdapter()).showError();
        }
    }

    @Override
    public void onResultEmpty() {
        mPresenter.getNextRates();
    }

    @Override
    public void openCurrencyRateDetails(CurrencyItem currencyItem) {
        startActivity(RateDetailsActivity.getStartIntent(getContext(), currencyItem));
    }

    @Override
    public void setTitle(String title) {
        getActivity().setTitle(title);
    }

    @Override
    public void clearCurrencyList() {
        ((CurrencyRatesAdapter) mRecyclerView.getAdapter()).clear();
        mRecyclerView.setOnScrollListener(new InfiniteScrollListener(() -> mPresenter.getNextRates()));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, mRecyclerView.getLayoutManager().onSaveInstanceState());
    }
}
