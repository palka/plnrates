package com.nekodev.paulina.sadowska.currencyexchange.rates.adapter;

/**
 * Created by Paulina Sadowska on 05.08.2017.
 */

public interface ViewType {
    int getViewType();
}
