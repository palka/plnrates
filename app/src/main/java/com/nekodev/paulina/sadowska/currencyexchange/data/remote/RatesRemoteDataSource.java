package com.nekodev.paulina.sadowska.currencyexchange.data.remote;

import com.nekodev.paulina.sadowska.currencyexchange.api.FixerService;
import com.nekodev.paulina.sadowska.currencyexchange.data.RatesDataSource;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

import io.reactivex.Flowable;

/**
 * Created by Paulina Sadowska on 06.08.2017.
 */
@Singleton
public class RatesRemoteDataSource implements RatesDataSource {

    private final FixerService mService;

    public RatesRemoteDataSource(FixerService service) {
        this.mService = service;
    }

    @Override
    public Flowable<List<CurrencyItem>> getCurrencyRates(String date) {
        return mService.getCurrencyRates(date)
                .map(ratesForDate -> {
                    if (ratesForDate.getDate().equals(date)) {
                        List<CurrencyItem> currencies = new ArrayList<>();
                        for(String name : ratesForDate.getRates().keySet()) {
                            Double rate = ratesForDate.getRates().get(name);
                            currencies.add(new CurrencyItem(date, name, rate));
                        }
                        return currencies;
                    }
                    return Collections.emptyList();
                });
    }

    @Override
    public void saveCurrencyRates(String date, List<CurrencyItem> currencyItems) {

    }

    @Override
    public Flowable<Boolean> containsDate(String date) {
        return Flowable.just(true);
    }
}
