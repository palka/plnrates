package com.nekodev.paulina.sadowska.currencyexchange.data.local;

import com.nekodev.paulina.sadowska.currencyexchange.data.RatesDataSource;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Paulina Sadowska on 07.08.2017.
 */

public class RatesLocalDataSource implements RatesDataSource {

    @Override
    public Flowable<List<CurrencyItem>> getCurrencyRates(String date) {
        try (Realm realmInstance = Realm.getDefaultInstance()) {
            RealmResults<CurrencyRateRealm> realmResults = realmInstance.where(CurrencyRateRealm.class).equalTo("date", date).findAll();
            if (realmResults.isEmpty()) {
                return Flowable.empty();
            }
            List<CurrencyItem> results = new ArrayList<>();
            for (CurrencyRateRealm result : realmResults) {
                results.add(new CurrencyItem(result.getDate(), result.getName(), result.getRate()));
            }
            return Flowable.just(results);
        }
    }

    @Override
    public void saveCurrencyRates(String date, List<CurrencyItem> currencyItems) {
        try (Realm realmInstance = Realm.getDefaultInstance()) {
            realmInstance.executeTransaction((realm) -> {
                realm.copyToRealm(new DateRealm(date));
                for (CurrencyItem item : currencyItems) {
                    if (realm.where(CurrencyRateRealm.class).equalTo("date", date).equalTo("name", item.getName()).findFirst() == null) {
                        realm.copyToRealm(new CurrencyRateRealm(date, item.getName(), item.getRate()));
                    }
                }
            });
        }
    }

    @Override
    public Flowable<Boolean> containsDate(String date) {
        try (Realm realmInstance = Realm.getDefaultInstance()) {
            if (realmInstance.where(DateRealm.class).equalTo("date", date).findFirst() != null) {
                return Flowable.just(true);
            }
            return Flowable.just(false);
        }

    }
}
