package com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.error;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nekodev.paulina.sadowska.currencyexchange.R;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewType;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewTypeDelegateAdapter;

/**
 * Created by Paulina Sadowska on 09.08.2017.
 */

public class ErrorDelegateAdapter implements ViewTypeDelegateAdapter {
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_error, parent, false);
        return new ErrorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, ViewType viewType) {

    }

    static class ErrorViewHolder extends RecyclerView.ViewHolder {

        public ErrorViewHolder(View view) {
            super(view);
        }
    }
}
