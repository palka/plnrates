package com.nekodev.paulina.sadowska.currencyexchange;

/**
 * Created by Paulina Sadowska on 04.08.2017.
 */

public interface BaseView<T extends BasePresenter> {
    void setPresenter(T presenter);
}
