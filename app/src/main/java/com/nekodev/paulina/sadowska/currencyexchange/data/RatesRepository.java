package com.nekodev.paulina.sadowska.currencyexchange.data;

import com.nekodev.paulina.sadowska.currencyexchange.data.local.Local;
import com.nekodev.paulina.sadowska.currencyexchange.data.remote.Remote;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

/**
 * Created by Paulina Sadowska on 04.08.2017.
 */
@Singleton
public class RatesRepository implements RatesDataSource {

    private final RatesDataSource mRemoteDataSource;
    private final RatesDataSource mLocalDataSource;
    private final LinkedHashMap<String, List<CurrencyItem>> mCachedRates;

    @Inject
    RatesRepository(@Local RatesDataSource localDataSource,
                    @Remote RatesDataSource remoteDataSource) {
        this.mRemoteDataSource = remoteDataSource;
        this.mLocalDataSource = localDataSource;
        this.mCachedRates = new LinkedHashMap<>();
    }

    @Override
    public Flowable<List<CurrencyItem>> getCurrencyRates(String date) {
        if (mCachedRates.containsKey(date)) {
            return Flowable.just(mCachedRates.get(date));
        }
        return Flowable.concat(getAndCacheLocalRates(date), getAndCacheRemoteRates(date))
                .filter(tasks -> !tasks.isEmpty())
                .first(new ArrayList<>())
                .doOnSuccess(currencyItems -> mCachedRates.put(date, currencyItems))
                .toFlowable();
    }

    @Override
    public void saveCurrencyRates(String date, List<CurrencyItem> currencyItems) {

    }

    @Override
    public Flowable<Boolean> containsDate(String date) {
        return mLocalDataSource.containsDate(date);
    }

    private Flowable<List<CurrencyItem>> getAndCacheLocalRates(String date) {

        return mLocalDataSource.getCurrencyRates(date)
                .flatMap(currencyItems -> Flowable.fromIterable(currencyItems)
                        .doOnComplete(() -> mCachedRates.put(date, currencyItems))
                        .toList().toFlowable());
    }

    private Flowable<List<CurrencyItem>> getAndCacheRemoteRates(String date) {

        return mRemoteDataSource.getCurrencyRates(date)
                .flatMap(currencyItems -> Flowable.fromIterable(currencyItems)
                        .doOnComplete(() -> {
                            if (!currencyItems.isEmpty()) {
                                mCachedRates.put(date, currencyItems);
                            }
                            mLocalDataSource.saveCurrencyRates(date, currencyItems);
                        })
                        .toList().toFlowable());
    }

    public Map<String, List<CurrencyItem>> getCachedRates() {
        return mCachedRates;
    }
}
