package com.nekodev.paulina.sadowska.currencyexchange.api;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Paulina Sadowska on 06.08.2017.
 */

public interface FixerService {

    //http://api.fixer.io/2017-08-15?base=PLN
    @GET("{date}?base=PLN")
    Flowable<RatesForDate> getCurrencyRates(@Path("date") String date);
}
