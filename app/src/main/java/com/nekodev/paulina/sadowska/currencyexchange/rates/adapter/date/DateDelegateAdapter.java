package com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.date;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.currencyexchange.R;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewType;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewTypeDelegateAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 05.08.2017.
 */

public class DateDelegateAdapter implements ViewTypeDelegateAdapter {
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_date, parent, false);
        return new DateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, ViewType viewType) {
        if (viewType instanceof DateItem && holder instanceof DateViewHolder) {
            ((DateViewHolder) holder).bind((DateItem) viewType);
        }
    }

    static class DateViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date_item_date)
        TextView mDate;

        DateViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        void bind(DateItem dateItem) {
            mDate.setText(dateItem.getDate());
        }
    }
}
