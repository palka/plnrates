package com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.currencyexchange.R;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewType;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewTypeDelegateAdapter;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 05.08.2017.
 */

public class CurrencyDelegateAdapter implements ViewTypeDelegateAdapter {

    private final OnViewSelectedListener mListener;

    public CurrencyDelegateAdapter(OnViewSelectedListener listener) {
        this.mListener = listener;
    }

    public interface OnViewSelectedListener {
        void onItemSelected(CurrencyItem currencyItem);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_currency_rate, parent, false);
        return new CurrencyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, ViewType viewType) {
        if (viewType instanceof CurrencyItem && holder instanceof CurrencyViewHolder) {
            ((CurrencyViewHolder) holder).bind((CurrencyItem) viewType);
        }
    }


    class CurrencyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.currency_item_name)
        TextView mName;
        @BindView(R.id.currency_item_rate)
        TextView mRate;
        @BindView(R.id.currency_item_layout)
        LinearLayout layout;

        CurrencyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        void bind(CurrencyItem currency) {
            mName.setText(currency.getName());
            mRate.setText(String.format(Locale.ENGLISH, "%.3f", currency.getRate()));
            layout.setOnClickListener(view -> mListener.onItemSelected(currency));
        }
    }
}
