package com.nekodev.paulina.sadowska.currencyexchange.data.local;

import io.realm.RealmObject;

/**
 * Created by Paulina Sadowska on 07.08.2017.
 */
@SuppressWarnings("unused")
public class CurrencyRateRealm extends RealmObject {
    private String date;
    private String name;
    private Double rate;

    public CurrencyRateRealm() {

    }

    public CurrencyRateRealm(String date, String name, Double rate) {
        this.date = date;
        this.name = name;
        this.rate = rate;
    }

    public Double getRate() {
        return rate;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
}
