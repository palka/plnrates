package com.nekodev.paulina.sadowska.currencyexchange.data;

import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Paulina Sadowska on 06.08.2017.
 */

public interface RatesDataSource {
    Flowable<List<CurrencyItem>> getCurrencyRates(String date);

    void saveCurrencyRates(String date, List<CurrencyItem> currencyItems);

    Flowable<Boolean> containsDate(String date);
}