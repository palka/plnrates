package com.nekodev.paulina.sadowska.currencyexchange.ratedetails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.currencyexchange.R;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 08.08.2017.
 */

public class RateDetailsFragment extends Fragment {

    private static final String ARGUMENT_CURRENCY_ITEM = "argumentCurrencyRate";

    @BindView(R.id.details_date)
    TextView mDate;

    @BindView(R.id.details_rate)
    TextView mRate;

    public static RateDetailsFragment newInstance(CurrencyItem currencyItem) {
        Bundle arguments = new Bundle();
        arguments.putParcelable(ARGUMENT_CURRENCY_ITEM, currencyItem);
        RateDetailsFragment fragment = new RateDetailsFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rate_details_fragment, container, false);
        ButterKnife.bind(this, view);
        CurrencyItem item = getArguments().getParcelable(ARGUMENT_CURRENCY_ITEM);
        if (item != null) {
            SimpleDateFormat displayFormat = new SimpleDateFormat("d MMMM yyyy", Locale.getDefault());
            SimpleDateFormat storeFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            try {
                mDate.setText(displayFormat.format(storeFormat.parse(item.getDate())));
            } catch (ParseException e) {
                Log.e(RateDetailsFragment.class.getSimpleName(), e.getMessage());
            }
            mRate.setText(String.format(Locale.ENGLISH, "%.4f", item.getRate()));
        }
        return view;
    }
}
