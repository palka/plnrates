package com.nekodev.paulina.sadowska.currencyexchange.rates.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyDelegateAdapter;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.date.DateDelegateAdapter;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.date.DateItem;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.error.ErrorDelegateAdapter;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.error.ErrorItem;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.loading.LoadingDelegateAdapter;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.loading.LoadingItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paulina Sadowska on 05.08.2017.
 */

public class CurrencyRatesAdapter extends RecyclerView.Adapter {

    private final ArrayList<ViewType> items;
    private final SparseArray<ViewTypeDelegateAdapter> delegateAdapters;
    private final LoadingItem loadingItem;

    public CurrencyRatesAdapter(CurrencyDelegateAdapter.OnViewSelectedListener listener) {
        this.loadingItem = new LoadingItem();
        delegateAdapters = new SparseArray<>();
        delegateAdapters.put(AdapterConstants.LOADING, new LoadingDelegateAdapter());
        delegateAdapters.put(AdapterConstants.CURRENCY, new CurrencyDelegateAdapter(listener));
        delegateAdapters.put(AdapterConstants.DATE, new DateDelegateAdapter());
        delegateAdapters.put(AdapterConstants.ERROR, new ErrorDelegateAdapter());
        items = new ArrayList<>();
        items.add(loadingItem);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return delegateAdapters.get(viewType).onCreateViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position).getViewType();
    }

    public void addItems(DateItem dateItem, List<CurrencyItem> currencies) {
        int initPosition = 0;
        if (!items.isEmpty()) {
            initPosition = items.size() - 1;
            items.remove(initPosition);
            notifyItemRemoved(initPosition);
        }

        items.add(dateItem);
        items.addAll(currencies);
        items.add(loadingItem);
        notifyItemRangeInserted(initPosition, currencies.size() + 2);
    }

    public ViewType getItem(int position) {
        return items.get(position);
    }

    public void showError() {
        int initPosition = 0;
        if (!items.isEmpty()) {
            initPosition = items.size() - 1;
            items.remove(initPosition);
            notifyItemRemoved(initPosition);
        }
        items.add(new ErrorItem());
        notifyItemInserted(initPosition);
    }

    public void clear() {
        int size = items.size();
        items.clear();
        notifyItemRangeRemoved(0, size);
    }
}
