package com.nekodev.paulina.sadowska.currencyexchange.data.local;

import io.realm.RealmObject;

/**
 * Created by Paulina Sadowska on 09.08.2017.
 */

public class DateRealm extends RealmObject {
    private String date;

    public DateRealm() {

    }

    public DateRealm(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
