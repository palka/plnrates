package com.nekodev.paulina.sadowska.currencyexchange.rates;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.nekodev.paulina.sadowska.currencyexchange.data.RatesRepository;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewType;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.date.DateItem;
import com.nekodev.paulina.sadowska.currencyexchange.utils.schedulers.BaseSchedulerProvider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Paulina Sadowska on 04.08.2017.
 */

class CurrencyRatesPresenter implements CurrencyRatesContract.Presenter {

    private final CurrencyRatesContract.View mView;
    private final RatesRepository mRatesRepository;
    private final BaseSchedulerProvider mSchedulerProvider;
    private final CompositeDisposable mDisposable;
    private final SimpleDateFormat mDisplayedDateFormat;
    private final SimpleDateFormat mStoredDateFormat;
    private final Date mToday;

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    Date mLatestDate;


    @Inject
    CurrencyRatesPresenter(@NonNull CurrencyRatesContract.View view,
                           @NonNull RatesRepository ratesRepository,
                           @NonNull BaseSchedulerProvider schedulerProvider,
                           @NonNull Date today,
                           @Named("stored") SimpleDateFormat storedDateFormat,
                           @Named("displayed") SimpleDateFormat displayedDateFormat) {
        this.mView = view;
        this.mRatesRepository = ratesRepository;
        this.mSchedulerProvider = schedulerProvider;
        this.mDisposable = new CompositeDisposable();
        this.mToday = today;
        this.mLatestDate = today;
        this.mStoredDateFormat = storedDateFormat;
        this.mDisplayedDateFormat = displayedDateFormat;
    }

    @Inject
    void setupListeners() {
        mView.setPresenter(this);
    }


    @Override
    public void subscribe() {
        getNextRates();
    }

    @Override
    public void unSubscribe() {
        mDisposable.clear();
    }

    @Override
    public void getNextRates() {
        String date = mStoredDateFormat.format(mLatestDate);
        mDisposable.add(mRatesRepository.getCurrencyRates(date)
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        currencies -> {
                            mLatestDate = subtractDay(mLatestDate);
                            if (!currencies.isEmpty()) {
                                mView.addCurrencyRates(formatStoredDateToDisplayed(currencies.get(0).getDate()), currencies);
                            } else {
                                mView.onResultEmpty();
                            }
                        },
                        error -> mDisposable.add(mRatesRepository.containsDate(mStoredDateFormat.format(mLatestDate))
                                .subscribeOn(mSchedulerProvider.computation())
                                .observeOn(mSchedulerProvider.ui())
                                .subscribe(
                                        result -> {
                                            if (result) {
                                                mLatestDate = subtractDay(mLatestDate);
                                                getNextRates();
                                            } else {
                                                mView.showError();
                                            }
                                        }
                                )
                        )
                ));
    }


    @Override
    public void currencyItemSelected(CurrencyItem currencyItem) {
        mView.openCurrencyRateDetails(currencyItem);
    }

    @Override
    public void addCachedRates() {
        Map<String, List<CurrencyItem>> cachedDates = mRatesRepository.getCachedRates();
        for (String date : cachedDates.keySet()) {
            List<CurrencyItem> currencyItems = cachedDates.get(date);
            try {
                mLatestDate = mStoredDateFormat.parse(date);
                if (!currencyItems.isEmpty()) {
                    mView.addCurrencyRates(mDisplayedDateFormat.format(mLatestDate), currencyItems);
                }
            } catch (ParseException e) {
                mView.showError();
            }
        }
        if (!cachedDates.isEmpty()) {
            mLatestDate = subtractDay(mLatestDate);
        }
    }

    @Override
    public void scrolledToItem(ViewType item) {
        String date = "";
        if (item instanceof CurrencyItem) {
            date = formatStoredDateToDisplayed(((CurrencyItem) item).getDate());
        } else if (item instanceof DateItem) {
            date = formatStoredDateToDisplayed(((DateItem) item).getDate());
        }
        if (!date.isEmpty()) {
            mView.setTitle(date);
        }
    }

    @Override
    public void refresh() {
        mView.clearCurrencyList();
        mDisposable.clear();
        mLatestDate = mToday;
        getNextRates();
    }

    private String formatStoredDateToDisplayed(String date) {
        try {
            return mDisplayedDateFormat.format(mStoredDateFormat.parse(date));
        } catch (ParseException e) {
            return "";
        }
    }

    private static Date subtractDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        return cal.getTime();
    }
}
