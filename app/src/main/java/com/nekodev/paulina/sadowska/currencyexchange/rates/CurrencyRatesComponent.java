package com.nekodev.paulina.sadowska.currencyexchange.rates;

import com.nekodev.paulina.sadowska.currencyexchange.data.RatesRepositoryComponent;
import com.nekodev.paulina.sadowska.currencyexchange.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by Paulina Sadowska on 04.08.2017.
 */
@FragmentScope
@Component(dependencies = RatesRepositoryComponent.class, modules = CurrencyRatesModule.class)
public interface CurrencyRatesComponent {
    void inject(CurrencyRatesActivity currencyRatesActivity);
}
