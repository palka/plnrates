package com.nekodev.paulina.sadowska.currencyexchange.rates.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by Paulina Sadowska on 05.08.2017.
 */

public interface ViewTypeDelegateAdapter {
    RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent);

    void onBindViewHolder(RecyclerView.ViewHolder holder, ViewType viewType);
}
