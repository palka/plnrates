package com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.error;

import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.AdapterConstants;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewType;

/**
 * Created by Paulina Sadowska on 09.08.2017.
 */

public class ErrorItem implements ViewType {
    @Override
    public int getViewType() {
        return AdapterConstants.ERROR;
    }
}
