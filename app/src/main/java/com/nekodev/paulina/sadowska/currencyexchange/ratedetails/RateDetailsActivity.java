package com.nekodev.paulina.sadowska.currencyexchange.ratedetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.nekodev.paulina.sadowska.currencyexchange.R;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;
import com.nekodev.paulina.sadowska.currencyexchange.utils.ActivityUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 08.08.2017.
 */

public class RateDetailsActivity extends AppCompatActivity {

    private static final String CURRENCY_ITEM_EXTRA = "currencyItemExtra";

    @BindView(R.id.details_toolbar)
    Toolbar mToolbar;

    public static Intent getStartIntent(Context context, CurrencyItem currencyItem) {
        return new Intent(context, RateDetailsActivity.class)
                .putExtra(CURRENCY_ITEM_EXTRA, currencyItem);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rate_details_activity);
        ButterKnife.bind(this);

        CurrencyItem currencyItem = getIntent().getParcelableExtra(CURRENCY_ITEM_EXTRA);
        mToolbar.setTitle("PLN ➙ " + currencyItem.getName());

        RateDetailsFragment ratesFragment = (RateDetailsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (ratesFragment == null) {
            ratesFragment = RateDetailsFragment.newInstance(currencyItem);

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    ratesFragment, R.id.contentFrame);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
