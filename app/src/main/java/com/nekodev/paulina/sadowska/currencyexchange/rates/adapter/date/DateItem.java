package com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.date;

import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.AdapterConstants;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewType;

/**
 * Created by Paulina Sadowska on 05.08.2017.
 */

public class DateItem implements ViewType {

    private final String date;

    public DateItem(String date) {
        this.date = date;
    }

    @Override
    public int getViewType() {
        return AdapterConstants.DATE;
    }

    public String getDate() {
        return date;
    }
}
