package com.nekodev.paulina.sadowska.currencyexchange.rates;

import com.nekodev.paulina.sadowska.currencyexchange.utils.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.currencyexchange.utils.schedulers.SchedulerProvider;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 04.08.2017.
 */

@Module
class CurrencyRatesModule {

    private final CurrencyRatesContract.View mView;

    CurrencyRatesModule(CurrencyRatesContract.View view) {
        this.mView = view;
    }

    @Provides
    BaseSchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider();
    }

    @Provides
    Date providesTodayDate() {
        return Calendar.getInstance().getTime();
    }

    @Provides
    CurrencyRatesContract.View providesView() {
        return mView;
    }

    @Provides
    @Named("stored")
    SimpleDateFormat providesStoredDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    }

    @Provides
    @Named("displayed")
    SimpleDateFormat providesDisplayedDateFormat() {
        return new SimpleDateFormat("d MMMM yyyy", Locale.getDefault());
    }
}
