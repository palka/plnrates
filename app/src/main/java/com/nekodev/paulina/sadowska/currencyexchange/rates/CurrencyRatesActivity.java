package com.nekodev.paulina.sadowska.currencyexchange.rates;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nekodev.paulina.sadowska.currencyexchange.CurrencyApplication;
import com.nekodev.paulina.sadowska.currencyexchange.R;
import com.nekodev.paulina.sadowska.currencyexchange.utils.ActivityUtils;

import javax.inject.Inject;

public class CurrencyRatesActivity extends AppCompatActivity {

    @Inject
    CurrencyRatesPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.currency_rates_activity);

        CurrencyRatesFragment ratesFragment = (CurrencyRatesFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);
        if (ratesFragment == null) {
            ratesFragment = CurrencyRatesFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    ratesFragment, R.id.contentFrame);
        }
        DaggerCurrencyRatesComponent.builder()
                .ratesRepositoryComponent(((CurrencyApplication) getApplication()).getRepositoryComponent())
                .currencyRatesModule(new CurrencyRatesModule(ratesFragment))
                .build()
                .inject(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
