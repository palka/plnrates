package com.nekodev.paulina.sadowska.currencyexchange;

import android.app.Application;

import com.nekodev.paulina.sadowska.currencyexchange.data.DaggerRatesRepositoryComponent;
import com.nekodev.paulina.sadowska.currencyexchange.data.RatesRepositoryComponent;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Paulina Sadowska on 04.08.2017.
 */

public class CurrencyApplication extends Application {

    private RatesRepositoryComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.setDefaultConfiguration(new RealmConfiguration.Builder(this).build());
        mComponent = DaggerRatesRepositoryComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .build();

    }

    public RatesRepositoryComponent getRepositoryComponent() {
        return mComponent;
    }
}
