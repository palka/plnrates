package com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.loading;

import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.AdapterConstants;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.ViewType;

/**
 * Created by Paulina Sadowska on 05.08.2017.
 */

public class LoadingItem implements ViewType {
    @Override
    public int getViewType() {
        return AdapterConstants.LOADING;
    }
}
