package com.nekodev.paulina.sadowska.currencyexchange;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.nekodev.paulina.sadowska.currencyexchange.ratedetails.RateDetailsActivity;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Paulina Sadowska on 09.08.2017.
 */
@RunWith(AndroidJUnit4.class)
public class RateDetailsActivityTest {

    @Rule
    public ActivityTestRule<RateDetailsActivity> mActivityRule =
            new ActivityTestRule<RateDetailsActivity>(RateDetailsActivity.class) {
                @Override
                protected Intent getActivityIntent() {
                    Context targetContext = InstrumentationRegistry.getInstrumentation()
                            .getTargetContext();
                    return RateDetailsActivity.getStartIntent(targetContext, new CurrencyItem("2017-06-12", "AUD", 0.1234));
                }
            };

    @Test
    public void openActivityWithExtras_rateDetailsDisplayed() {
        onView(withId(R.id.details_rate)).check(matches(withText("0.1234")));
        onView(withId(R.id.details_date)).check(matches(isDisplayed()));
        onView(withId(R.id.details_toolbar)).check(matches(isDisplayed()));
    }
}
