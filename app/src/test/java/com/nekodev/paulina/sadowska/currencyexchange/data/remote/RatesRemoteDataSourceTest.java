package com.nekodev.paulina.sadowska.currencyexchange.data.remote;

import com.nekodev.paulina.sadowska.currencyexchange.api.RatesForDate;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by Paulina Sadowska on 07.08.2017.
 */
public class RatesRemoteDataSourceTest {

    @Test
    public void getCurrencyRates() throws Exception {
        Map<String, Double> rates = createCurrencyRatesMap(10);
        RatesRemoteDataSource mRemote = new RatesRemoteDataSource(date -> {
            RatesForDate ratesForDate = Mockito.mock(RatesForDate.class);
            when(ratesForDate.getDate()).thenReturn(date);
            when(ratesForDate.getRates()).thenReturn(rates);
            return Flowable.just(ratesForDate);
        });
        List<CurrencyItem> result = new ArrayList<>();
        mRemote.getCurrencyRates("date").subscribe(result::addAll,
                e -> fail(e.getMessage()));
        assertEquals(rates.size(), result.size());

        for (CurrencyItem currencyItem : result) {
            assertTrue(rates.containsKey(currencyItem.getName()));
            assertEquals(rates.get(currencyItem.getName()), currencyItem.getRate(), 0.01);
        }
    }

    private Map<String, Double> createCurrencyRatesMap(int size) {
        Map<String, Double> ratesMap = new HashMap<>();
        for (int i = 0; i < size; i++) {
            ratesMap.put("XX" + i, 0.01 * i);
        }
        return ratesMap;
    }

}