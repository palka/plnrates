package com.nekodev.paulina.sadowska.currencyexchange.data;

import com.nekodev.paulina.sadowska.currencyexchange.data.local.RatesLocalDataSource;
import com.nekodev.paulina.sadowska.currencyexchange.data.remote.RatesRemoteDataSource;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by Paulina Sadowska on 07.08.2017.
 */
public class RatesRepositoryTest {

    private RatesRepository mRatesRepository;

    private RatesDataSource mRemote;
    private RatesDataSource mLocal;

    @Before
    public void setUp() {
        mRemote = Mockito.mock(RatesRemoteDataSource.class);
        mLocal = Mockito.mock(RatesLocalDataSource.class);
        mRatesRepository = new RatesRepository(mLocal, mRemote);

    }

    @Test
    public void getCurrencyRates_LocalAndRemoteResults_returnsLocalResults() {
        List<CurrencyItem> localResult = createCurrencyItems(2);
        when(mLocal.getCurrencyRates(anyString())).thenReturn(Flowable.just(localResult));
        when(mRemote.getCurrencyRates(anyString())).thenReturn(Flowable.just(createCurrencyItems(7)));

        List<CurrencyItem> result = new ArrayList<>();
        mRatesRepository.getCurrencyRates("date").subscribe(result::addAll);
        assertEquals(localResult.size(), result.size());
    }

    @Test
    public void getCurrencyRates_onlyLocalResults_returnsLocalResults() {
        List<CurrencyItem> localResult = createCurrencyItems(2);
        when(mLocal.getCurrencyRates(anyString())).thenReturn(Flowable.just(localResult));
        when(mRemote.getCurrencyRates(anyString())).thenReturn(Flowable.just(new ArrayList<>()));

        List<CurrencyItem> result = new ArrayList<>();
        mRatesRepository.getCurrencyRates("date").subscribe(result::addAll);
        assertEquals(localResult.size(), result.size());
    }

    @Test
    public void getCurrencyRates_onlyRemoteResults_returnsRemoteResults() {
        when(mLocal.getCurrencyRates(anyString())).thenReturn(Flowable.just(new ArrayList<>()));
        List<CurrencyItem> remoteResults = createCurrencyItems(7);
        when(mRemote.getCurrencyRates(anyString())).thenReturn(Flowable.just(remoteResults));

        List<CurrencyItem> result = new ArrayList<>();
        mRatesRepository.getCurrencyRates("date").subscribe(result::addAll);
        assertEquals(remoteResults.size(), result.size());
    }

    @Test
    public void getCurrencyRates_cachedResults_returnsCachedResults() {
        List<CurrencyItem> remoteResults = createCurrencyItems(7);
        when(mRemote.getCurrencyRates(anyString())).thenReturn(Flowable.just(remoteResults));
        when(mLocal.getCurrencyRates(anyString())).thenReturn(Flowable.just(new ArrayList<>()));

        List<CurrencyItem> result = new ArrayList<>();
        mRatesRepository.getCurrencyRates("date").subscribe();

        when(mRemote.getCurrencyRates(anyString())).thenReturn(Flowable.just(new ArrayList<>()));

        mRatesRepository.getCurrencyRates("date").subscribe(result::addAll);
        assertEquals(remoteResults.size(), result.size());
    }

    private List<CurrencyItem> createCurrencyItems(int size) {
        List<CurrencyItem> currencies = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            currencies.add(new CurrencyItem("", "XX" + i, 0.01 * i));
        }
        return currencies;
    }


}