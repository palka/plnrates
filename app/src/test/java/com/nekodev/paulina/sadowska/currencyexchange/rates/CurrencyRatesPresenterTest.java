package com.nekodev.paulina.sadowska.currencyexchange.rates;

import com.nekodev.paulina.sadowska.currencyexchange.data.RatesRepository;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.currency.CurrencyItem;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.date.DateItem;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.error.ErrorItem;
import com.nekodev.paulina.sadowska.currencyexchange.rates.adapter.loading.LoadingItem;
import com.nekodev.paulina.sadowska.currencyexchange.utils.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.currencyexchange.utils.schedulers.ImmediateSchedulerProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.reactivex.Flowable;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Paulina Sadowska on 06.08.2017.
 */
public class CurrencyRatesPresenterTest {

    private static final String TODAY = "2017-07-04";
    private static final String YESTERDAY = "2017-07-03";
    private static final String DAY_BEFORE_YESTERDAY = "2017-07-02";

    private CurrencyRatesPresenter mPresenter;
    private BaseSchedulerProvider mSchedulerProvider;
    private Date mToday;
    @Mock
    private CurrencyRatesContract.View mView;
    @Mock
    private RatesRepository mDataSource;
    private SimpleDateFormat mDateFormat;
    private List<CurrencyItem> mCurrencies;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mSchedulerProvider = new ImmediateSchedulerProvider();
        mToday = createTodayDate();
        mCurrencies = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            mCurrencies.add(new CurrencyItem("", "XX" + i, 0.15 * i));
        }
        when(mDataSource.getCurrencyRates(anyString())).thenReturn(Flowable.just(mCurrencies));
        when(mDataSource.getCachedRates()).thenReturn(new HashMap<>());
        when(mDataSource.containsDate(anyString())).thenReturn(Flowable.just(false));
        mDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    }

    private Date createTodayDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 6, 4);
        return calendar.getTime();
    }

    @Test
    public void subscribe_currencyRatesAdded_dateSetForDayBefore() {
        mPresenter = new CurrencyRatesPresenter(mView, mDataSource, mSchedulerProvider, mToday, mDateFormat, mDateFormat);
        mPresenter.subscribe();
        verify(mView).addCurrencyRates(anyString(), eq(mCurrencies));
        assertEquals(YESTERDAY, mDateFormat.format(mPresenter.mLatestDate));
    }

    @Test
    public void subscribe_emptyResults_onResultEmptyCalled() {
        when(mDataSource.getCurrencyRates(anyString())).thenReturn(Flowable.just(new ArrayList<>()));
        mPresenter = new CurrencyRatesPresenter(mView, mDataSource, mSchedulerProvider, mToday, mDateFormat, mDateFormat);
        mPresenter.subscribe();
        verify(mView, times(0)).addCurrencyRates(anyString(), anyList());
        assertEquals(YESTERDAY, mDateFormat.format(mPresenter.mLatestDate));
        verify(mView).onResultEmpty();
    }

    @Test
    public void getNextRates_currencyRatesForYesterdayAdded() {
        mPresenter = new CurrencyRatesPresenter(mView, mDataSource, mSchedulerProvider, mToday, mDateFormat, mDateFormat);
        mPresenter.subscribe();
        mPresenter.getNextRates();
        verify(mView, times(2)).addCurrencyRates(anyString(), eq(mCurrencies));
        assertEquals(DAY_BEFORE_YESTERDAY, mDateFormat.format(mPresenter.mLatestDate));
    }

    @Test
    public void subscribe_ratesReturnsError_repositoryContainsDate_errorNotShown() {
        when(mDataSource.getCurrencyRates(TODAY)).thenReturn(Flowable.error(new Throwable()));
        when(mDataSource.containsDate(TODAY)).thenReturn(Flowable.just(true));
        mPresenter = new CurrencyRatesPresenter(mView, mDataSource, mSchedulerProvider, mToday, mDateFormat, mDateFormat);
        mPresenter.subscribe();
        verify(mView, times(0)).showError();
        verify(mView).addCurrencyRates(anyString(), eq(mCurrencies));
        assertEquals(DAY_BEFORE_YESTERDAY, mDateFormat.format(mPresenter.mLatestDate));
    }

    @Test
    public void refresh_clearCurrencyListCalled_getNextRatesCalled() {
        mPresenter = new CurrencyRatesPresenter(mView, mDataSource, mSchedulerProvider, mToday, mDateFormat, mDateFormat);
        mPresenter.getNextRates();
        mPresenter.getNextRates();
        mPresenter.refresh();
        verify(mView).clearCurrencyList();
        assertEquals(YESTERDAY, mDateFormat.format(mPresenter.mLatestDate));
    }

    @Test
    public void scrolledToItem_currencyItem_setTitleCalled(){
        mPresenter = new CurrencyRatesPresenter(mView, mDataSource, mSchedulerProvider, mToday, mDateFormat, mDateFormat);
        mPresenter.scrolledToItem(new CurrencyItem(TODAY, "", 0.1));
        verify(mView).setTitle(TODAY);
    }

    @Test
    public void scrolledToItem_dateItem_setTitleCalled(){
        mPresenter = new CurrencyRatesPresenter(mView, mDataSource, mSchedulerProvider, mToday, mDateFormat, mDateFormat);
        mPresenter.scrolledToItem(new DateItem(YESTERDAY));
        verify(mView).setTitle(YESTERDAY);
    }

    @Test
    public void scrolledToItem_errorAndLoadingItem_setTitleNotCalled(){
        mPresenter = new CurrencyRatesPresenter(mView, mDataSource, mSchedulerProvider, mToday, mDateFormat, mDateFormat);
        mPresenter.scrolledToItem(new ErrorItem());
        mPresenter.scrolledToItem(new LoadingItem());
        verify(mView, times(0)).setTitle(anyString());
    }

}