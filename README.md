# PLNExchangeRates

App written in Java, which fetches PLN exchange rates from [Fixer.io API](http://fixer.io/) and displays the data on the list. Clicking on the item opens details screen.

Uses MVP architecture with Dagger 2 and RxJava. Main screen of the app consists of one RecyclerView with multiple delegate adapters which bind different view types (currency rate and date).

### References

- [Keddit — Part 4: RecyclerView — Delegate Adapters & Data Classes with Kotlin](https://android.jlelse.eu/keddit-part-4-recyclerview-delegate-adapters-data-classes-with-kotlin-9248f44327f7)

<p align="center">
    <img src="screenshots/list.png" alt="App screenshot - rates list" height="600" />
    <img src="screenshots/details.png" alt="App screenshot - details" height="600" />
</p>
